let gulp = require("gulp");
let blade = require("./");

gulp.task("page", () => {
    return gulp.src("tests/source/index.blade")
        .pipe(blade())
        .pipe(gulp.dest("tests/results"));
});